This is a small demonstration of modern CI/CD tooling, shamelessly borrowed and adapted from Rob van der Leek

Rob's great article, "How to build a modern CI/CD pipeline", is located at:
https://medium.com/bettercode/how-to-build-a-modern-ci-cd-pipeline-5faa01891a5b

Original Github Repo: https://github.com/robvanderleek/cicd-buzz
